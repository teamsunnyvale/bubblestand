from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from api.views import UserList, UserDetails, UserDetailsU, BubbleDetails, BubbleCreate 

user_list = UserList.as_view()
user_details = UserDetails.as_view()
user_details_u = UserDetailsU.as_view()
bubble_details = BubbleDetails.as_view()
bubble_create = BubbleCreate.as_view()
bubble_upload = "UploadBubble"
inbox_from = "InboxFrom"
inbox_all = "InboxAll"
inbox_dump = "InboxDump"

urlpatterns = format_suffix_patterns(patterns('api.views',
    url(r'^$','api_root'),

    url(r'^login/$', "Login", name="a_login"),
    url(r'^logout/$', "Logout", name="a_logout"),
    url(r'^signup/$', "CreateUser", name="a_signup"),

    url(r'^users/$',user_list,name="a_user_list"),
    url(r'^users/u/(?P<username>.+)/$',user_details_u,name="a_user_details_u"),
    url(r'^users/(?P<id>\d+)/$',user_details,name="a_user_details"),

    url(r'^bubble/(?P<bubbleid>\d+)/$',bubble_details, name="a_bubble_details"),
    url(r'^bubble/(?P<bubble_id>\d+)/view/$', "BubbleView", name="a_bubble_view"),
    url(r'^bubble/(?P<bubble_id>\d+)/upload/$',bubble_upload, name="a_bubble_upload"),
    url(r'^bubble/create/$', bubble_create, name="a_bubble_create"),

    url(r'^inbox/from/(?P<username>.+)/$', inbox_from, name="a_inbox_from"),
    url(r'^inbox/$', inbox_all, name="a_inbox_all"),
    url(r'^inbox/dump/$', inbox_dump, name="a_inbox_dump"),
))
