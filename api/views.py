from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.reverse import reverse
from rest_framework import generics,status
from rest_framework.permissions import IsAdminUser,IsAuthenticated
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.shortcuts import get_object_or_404

from api.serializers import *
from bubble.models import *
from bubblestand.settings import BUBBLE_UPLOAD_LOCATION, AWS_ACCESS_KEY, AWS_SECRET_KEY, S3_BUCKET

import os

#import boto
from boto.s3.key import Key
from boto.s3.connection import S3Connection

# Create your views here.
@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('a_user_list', request=request, format=format),
    })
    
class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)

class UserDetailsU(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = "username"

class UserDetails(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"

class BubbleDetails(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,bubbleid,format=None):
        bubble = get_object_or_404(Bubble,id=bubbleid)

        if request.user.id == bubble.sender.id or request.user.id == bubble.receiver.id:
            return Response(BubbleSerializer(bubble).data)
        else:
            return Response({"Reason":"Access unauthorized"}, status=status.HTTP_403_FORBIDDEN)

class BubbleCreate(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,format=None):
        post = request.DATA.copy()
        post["sender"] = request.user.id

        # Receiver will be a username, so fix that
        # TODO: Check if receiver is valid
        receiver = User.objects.get(username=post["receiver"])
        post["receiver"] = receiver.id
        
        bubble = BubbleSerializer(data=post)
        if bubble.is_valid():
            bubble.object.save()
            return Response({"Reason":"Successful","id":bubble.object.id},
                status=status.HTTP_201_CREATED)
        else:
            return Response({"Reason": "Input invalid"}, status=status.HTTP_400_BAD_REQUEST)

# Returns all bubbles received from one person
# TODO: Merge a user's sent messages to that person into the same QuerySet
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def InboxFrom(request, username):
    bubbles = request.user.received.filter(sender=get_object_or_404(User, username=username).id)
    
    if not bubbles:
        return Response({"Reason":"Incorrect username"}, status=status.HTTP_404_NOT_FOUND)
    
    bubbles = bubbles.order_by("sent")
    return Response(BubbleSerializer(bubbles,many=True).data)

# Returns list of last bubbles the auth'd user got messages from, ordered by time.
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def InboxAll(request):
    senders = request.user.received.values_list("sender",flat=True).distinct() #.distinct("sender")

    result = []
    for sender in senders:
        last_bubble = Bubble.objects.filter(sender=sender,receiver=request.user.id).order_by("-sent")[0]
        result += [{"sender":sender,"sent":last_bubble.sent}]

    return Response(result)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def InboxDump(request):
    bubbles = Bubble.objects.filter(receiver=request.user.id, seen=False)
    return Response(BubbleSerializer(bubbles,many=True).data)

# Check for user's password, create token, then return it
@api_view(['POST'])
def Login(request):
    user = get_object_or_404(User, username=request.DATA["username"])
    
    if check_password(request.DATA["password"],user.password) == True:
        token = None
        # Login successful, return username and token
        try:
            token = Token.objects.get(user=user)
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)
            token.save()

        response = {"username":user.username,"token":token.key}
        return Response(response)

    else:
        # Login failed, return HTTP 401
        return Response({"Reason":"Incorrect password"},status=status.HTTP_401_UNAUTHORIZED)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def Logout(request):
    # Just delete the token, and inform the client
    username = request.user.username
    request.auth.delete()
    
    return Response({"username":username, "logout":"true"})

# Values to provide: username, password, first_name, last_name, email
@api_view(['POST'])
def CreateUser(request):
    # TODO: Validate form input
    username    = request.DATA["username"]
    password    = request.DATA["password"]
    first_name  = request.DATA["first_name"]
    last_name   = request.DATA["last_name"]
    email       = request.DATA["email"]

    user = User.objects.create_user(username, email, password)
    user.first_name = first_name
    user.last_name = last_name

    user.save()

    token = Token.objects.create(user=user)
    response = {"username":user.username,"token":token.key}
    return Response(response)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def UploadBubble(request, bubble_id):
    bubble = get_object_or_404(Bubble, id=bubble_id)

    f = request.FILES["file"]
    file_endpoint = request.user.username + "_" + bubble_id  # TODO: Possibly make this more secure

    #dest_url = os.path.join(BUBBLE_UPLOAD_LOCATION, file_endpoint)
    #destination = open(dest_url, "wb+")

    #for chunk in f.chunks():
    #    destination.write(chunk)
    
    conn = S3Connection(AWS_ACCESS_KEY, AWS_SECRET_KEY)
    bucket = conn.get_bucket(S3_BUCKET)

    key = Key(bucket)
    key.key = file_endpoint
    
    key.set_contents_from_file(f, reduced_redundancy=True)

    bubble.filename = file_endpoint
    bubble.save()

    response = {"Reason":"Successful"}
    return Response(response)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def BubbleView(request, bubble_id):
    bubble = get_object_or_404(Bubble, id=bubble_id)

    if bubble.receiver.id != request.user.id:
        return Response({"Reason":"Unauthorized"}, status=status.HTTP_401_UNAUTHORIZED)

    if bubble.seen:
        return Response({"Reason":"Already seen"}, status=status.HTTP_410_GONE)

    conn = S3Connection(AWS_ACCESS_KEY, AWS_SECRET_KEY)
    bucket = conn.get_bucket(S3_BUCKET)
    key = bucket.get_key(bubble.filename)
    url = key.generate_url(600)

    bubble.seen = True
    bubble.save()

    response = {"id": bubble.id,
                "url": url}
    return Response(response)
    
