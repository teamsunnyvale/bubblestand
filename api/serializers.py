from rest_framework import serializers
from bubble.models import *
from django.contrib.auth.models import User

class BubbleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bubble
        fields = ('id','sender','receiver','filename','filetype')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email')
