from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Bubble(models.Model):
    AUDIO = "aud"
    VIDEO = "vid"
    TYPE_CHOICES=((AUDIO,"Audio"),(VIDEO,"Video"))

    sender = models.ForeignKey(User, related_name="sent")
    receiver = models.ForeignKey(User, related_name="received")
    filename = models.CharField(blank=True,max_length=100) # TODO: Remove "blank = True" when uploading is implemented
    filetype = models.CharField(max_length=3,
                                choices=TYPE_CHOICES,
                                default=AUDIO)
    seen = models.BooleanField(default=False)

    sent = models.DateTimeField(auto_now_add=True)
